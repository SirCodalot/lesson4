#define _CRT_SECURE_NO_WARNINGS

#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"
#include <iostream>

using namespace msl;

void testOutStream();
void testFileStream();
void testOutStreamEncrypted();
void testLogger();

int main(int argc, char **argv)
{

	testOutStream();
	testFileStream();
	testOutStreamEncrypted();
	testLogger();

	system("pause");
	return 0;
}

void testOutStream()
{
	OutStream* stream = new OutStream();

	*stream << "I am the doctor and I'm 1500 years old" << endline;
}

void testFileStream()
{
	FILE* file = fopen("test.txt", "w");
	FileStream* stream = new FileStream(file);

	*stream << "I am the doctor and I'm 1500 years old" << endline;
}

void testOutStreamEncrypted()
{
	OutStreamEncrypted* stream = new OutStreamEncrypted(3);

	*stream << "I am the doctor and I'm 1500 years old" << endline;
}

void testLogger()
{
	Logger* firstL = new Logger();
	Logger* secondL = new Logger();

	*firstL << "I am the doctor and I'm 1500 years old" << endline;
	*secondL << "I am the doctor and I'm 1500 years old" << endline;
	*firstL << "I am using the first logger" << endline;
	*secondL << "I am using the second logger" << endline;
}