#define _CRT_SECURE_NO_WARNINGS

#include "OutStreamEncrypted.h"

namespace msl
{

	OutStreamEncrypted::OutStreamEncrypted(int hist)
	{
		_hist = hist;
	}


	OutStream& OutStreamEncrypted::operator<<(const char* str)
	{
		int size = 0;
		for (size = 0; str[size]; size++);
		char* encrypted = new char[size];
		strcpy(encrypted, str);

		for (int i = 0; i < size; i++)
			encrypted[i] = encrypt(encrypted[i]);

		return OutStream::operator<<(encrypted);
	}

	OutStream& OutStreamEncrypted::operator<<(int num)
	{
		return OutStream::operator<<(encrypt(num));
	}

	int OutStreamEncrypted::encrypt(int value) const
	{
		for (int i = 0; i < _hist; i++)
		{
			value++;

			if (value > 126)
				value = 32;
		}
		return value;
	}

}