#pragma once
#include "OutStream.h"

namespace msl
{
	class FileStream : public OutStream
	{

	public:
		// Constructors and Destructors

		FileStream(FILE* file);

	};
}