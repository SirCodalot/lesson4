#pragma once

#include "OutStream.h"

namespace msl
{
	class OutStreamEncrypted : private OutStream
	{
	private:
		int _hist;

		int encrypt(int value) const;
	public:
		OutStreamEncrypted(int hist);

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
	};
}