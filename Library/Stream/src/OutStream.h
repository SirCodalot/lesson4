#pragma once

#include <iostream>

namespace msl
{
	class OutStream
	{
	protected:
		FILE * _file;

	public:
		// Constructors and Desturctors

		OutStream();
		~OutStream();

		// Getters and Setters

		FILE* getFile() const;
		void setFile(FILE* file);

		// Operators

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)(FILE* file));
	};

	void endline(FILE* file);
}