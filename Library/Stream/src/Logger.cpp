#include "Logger.h"

namespace msl
{
	Logger::Logger()
	{
		os = *new OutStream();
		_startLine = true;
	}

	Logger::~Logger()
	{
		delete(&os);
	}

	Logger& operator<<(Logger& l, const char* msg)
	{
		if (l._startLine)
			l.setStartLine();
		l.os << msg;
		return l;
	}

	Logger& operator<<(Logger& l, int num)
	{
		if (l._startLine)
			l.setStartLine();
		l.os << num;
		return l;
	}

	Logger& operator<<(Logger& l, void(*pf)(FILE* file))
	{
		l.os << pf;
		l._startLine = true;
		return l;
	}

	void Logger::setStartLine()
	{
		static unsigned int lines = 0;
		lines++;
		os << "LOG " << lines << " ";
		_startLine = false;
	}
}