#include "OutStream.h"

namespace msl
{
	OutStream::OutStream()
	{
		_file = stdout;
	}

	OutStream::~OutStream()
	{
		delete(_file);
	}

	FILE* OutStream::getFile() const
	{
		return _file;
	}

	void OutStream::setFile(FILE* file)
	{
		_file = file;
	}

	OutStream& OutStream::operator<<(const char *str)
	{
		fprintf(_file, "%s", str);
		return *this;
	}

	OutStream& OutStream::operator<<(int num)
	{
		fprintf(_file, "%d", num);
		return *this;
	}

	OutStream& OutStream::operator<<(void(*pf)(FILE* file))
	{
		pf(_file);
		return *this;
	}

	void endline(FILE* file)
	{
		fprintf(file, "\n");
	}
}