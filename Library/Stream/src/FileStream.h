#pragma once

#include "OutStream.h"
#include <iostream>

namespace msl
{
	class FileStream : public OutStream
	{

	public:
		FileStream(FILE* file);
	};
}